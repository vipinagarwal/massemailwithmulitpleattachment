import { LightningElement, track } from 'lwc';
import sendEmailMethod from '@salesforce/apex/EventMassEmailController.sendEmailMethod';
import getEventAttendees from '@salesforce/apex/EventMassEmailController.getEventAttendees';

import {
	showToast,
	handleError,
	getParamFromURL
} from 'c/pubsub';
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';
export default class BanEventMassEmail extends LightningElement {
    // fileData;
    formats = ['font', 'size', 'bold', 'italic', 'underline',
        'list', 'indent', 'align', 'link', 'clean', 'table', 'header'];
    filePresent = false;
    uploadedFiles = [];
    imageURl;
    file;
    fileContents;
    fileReader;
    content;
    fileName = '';
    MAX_FILE_SIZE = 5000000;
    @track  emailBody= '';
    @track showSpinner = false;
    @track emailSubject = '';
    @track data = {};
    connectedCallback(){
        this.eventId = getParamFromURL(window.location.href, 'id');
        this.data.volunteer = false;
        this.data.participants = false;
    }
    handleChange(event){
        if(event.target.type == "checkbox"){
            this.data[event.target.name] = event.target.checked ;
            this._getEventAttendees();
        }else{
            this.data[event.target.name] = event.target.value ;
        }
        if(this.data.volunteer == true ||  this.data.participants == true ){
            this.showViewButton = true;
        }else{
            this.showViewButton = false;
        }
    }
    handleDelete() {
        this.filePresent = false;
        this.fileData = null;
        this.fileContents = null;
        this.filesName ='';
    }
    filesName ='';
    extensionFile ;
    isPdf;
    attachmentArray = [];
	noAttendeesFound = false;
	attendees = [];

    openfileUpload(event) {
            try {
                this.filesName ='';
                console.log("handleOnFileUpload response-->" + JSON.stringify(event.target.files));
                let totalSize = 0;
                var response  = event.target.files;
                if(response.length > 0){
                    this.filePresent = true;
                }else{
                    this.filePresent = false;
                }
                if(response.length >10){
                    this.dispatchEvent(new ShowToastEvent({
                        message: 'You cannot select more than 10 files for Email',
                        variant: 'error'
                    }));
                    return;
                }
                this.attachmentArray = [];
                for (var i = 0; i < response.length; i++) {
                    const fileName = response[i].name;
                    this.filesName = this.filesName + fileName +', '
                    console.log('fileName --->' + fileName);
                    // let newfile = this.response[i].size;
                    totalSize = totalSize + response[i].size;
                    if (totalSize > this.MAX_FILE_SIZE) {
                        let filsSizeLimit = parseInt(this.MAX_FILE_SIZE) / 1000000;
                        // this.toastError("File Size Can not exceed : " + filsSizeLimit + ' MB');
                        this.dispatchEvent(new ShowToastEvent({
                            message: "File Size Can not exceed : " + filsSizeLimit + ' MB',
                            variant: 'error'
                        }));
                        this.handleDelete();
                    }
                    this.fileReader = new FileReader();
                    this.fileReader.onload =  ((e) => {
                        //  console.log( 'reader'+ reader.result);
                        let base64 = 'base64,';
                        let content = e.target.result.indexOf(base64) + base64.length;
                        let fileContents = e.target.result.substring(content);
                        let temp =  encodeURIComponent(fileContents) ;
                        let fileData = {
                            'Title':fileName,
                            'VersionData':  temp
                        };
                        this.attachmentArray.push(fileData);
                         console.log( this.attachmentArray); 
                        console.log('000=====>>'+this.attachmentArray.length); 

                    });
                    this.fileReader.readAsDataURL(response[i]);
                    console.log('response[i]'+response[i])
                }

                this.filesName = this.filesName.slice(0, -2);
                console.log('this.filesName[i]'+this.filesName)
                // console.log('000=====>>'+this.attachmentArray); 

            } catch (error) {
                console.error(error)
            } finally {
            
            }
           
    }
    eventId ='';
    showViewButton = false;
    sendEmail() {
        if(this.data.volunteer == false &&  this.data.participants == false ){
            this.dispatchEvent(new ShowToastEvent({
                message: 'Please Select atleast one to Send Email ',
                variant: 'error'
            }));
            return;
        }
        console.log('email');
        console.log('this.data'+this.data);
        console.log('000=====>>'+this.attachmentArray.length); 
        // console.log('000=====>>'+JSON.stringify(this.attachmentArray)); 

        let valid = false;
        valid = [
            ...this.template.querySelectorAll("lightning-input"),
            
        ].reduce(
            (validSoFar, input) => {
                input.reportValidity();
                return validSoFar && input.checkValidity();
            },
            true
        );
        console.log('valid');
        console.log(valid);

        if(valid ){
            this.showSpinner = true;
            if(this.attachmentArray.length > 0){
                this.filePresent = true;
            }else{
                this.filePresent = false;
            }
        
            sendEmailMethod({
                jsonData: JSON.stringify(this.data),
                eventId :this.eventId,
                fileList:this.attachmentArray, 
                filePresent: this.filePresent,
            })
            .then(result => {
                console.log('result'+JSON.stringify(result));
                if(result.message =='SUCCESS_MESSAGE'){
                    this.dispatchEvent(new ShowToastEvent({
                        title: 'Success',
                        message: 'Email Sended successfully',
                        variant: 'Success'
                    }));
                    location.reload();
                }else if (result.message =='NO_EMAIL'){
                    this.dispatchEvent(new ShowToastEvent({
                        title: 'Error',
                        message: 'There are no participant to Email',
                        variant: 'error'
                    }));
                }
                this.showSpinner = false;
            })
            .catch(error => {
                this.showSpinner = false;
                this.dispatchEvent(new ShowToastEvent({
                    title: 'Error',
                    message: 'There is some error please try again later',
                    variant: 'error'
                }));
                console.log('Error: sendMassEmail ===', JSON.stringify(error));
            });
        }
    }

    openModel =false;
    viewEmailList(){
        this.openModel =true;
    }
    closeEmailList(){
        this.openModel =false;
    }


    _getEventAttendees() {
		getEventAttendees({
				eventId: this.eventId,
			})
			.then((result) => {
               console.log('Attendees === ' + JSON.stringify(result));
                if(this.data.volunteer && this.data.participants ){
               console.log('Attendees 1=== ' + JSON.stringify(result));

                    this.attendees  = result;
                }else if (this.data.volunteer){
                    console.log('Attendees 2=== ' + JSON.stringify(result));
                    this.attendees  = result.filter(item => {
                        item.isVolunteer == true 
                   });
                }else if(this.data.participants){
                    console.log('Attendees 3=== ' + JSON.stringify(result));
                    this.attendees  = result.filter(item => 
                        item.isVolunteer == false 
                   );
                }
                
				if (this.attendees.length === 0) {
					this.noAttendeesFound = true;
					this.isAttendeeAvailable = false;
				} else {
					this.noAttendeesFound = false;
					this.isAttendeeAvailable = true;
					this.setPages(this.attendees);
				}
				this.showSpinner = false;
			})
			.catch((error) => {
				this.showSpinner = false;
				this.dispatchEvent(handleError(error));
			})
	}
    get currentPageData() {
		return this.pageData();
	}

	get hasPrev() {
		return this.page > 1;
	}

	get hasNext() {
		return this.page < this.pages.length;
	}

	get pagesList() {
		let mid = Math.floor(this.setSize / 2) + 1;
		if (this.page > mid) {
			return this.pages.slice(this.page - mid, this.page + mid - 1);
		}
		return this.pages.slice(0, this.setSize);
	}

	pageData = () => {
		let page = this.page;
		let perPage = this.perPage;
		let startIndex = page * perPage - perPage;
		let endIndex = page * perPage;
		return this.attendees.slice(startIndex, endIndex);
	};

	setPages = (data) => {
		let numberOfPages = Math.ceil(data.length / this.perPage);
		this.pages = [];
		for (let index = 1; index <= numberOfPages; index++) {
			this.pages.push(index);
		}

	};

	onPrev = () => {
		--this.page;
	};

	onNext = () => {
		++this.page;
	};

	onPageClick = (e) => {
		this.page = parseInt(e.target.dataset.id, 10);
    };
	@track pages = [];
	page = 1;
	setSize = 5;
	perPage = 10;
}