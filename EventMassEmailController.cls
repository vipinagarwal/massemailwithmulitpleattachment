@AuraEnabled
    public static List < AttendeeWrapper > getEventAttendees(String eventId ) {
        try {
            List < Event_Registration__c > eventRegList = new List < Event_Registration__c > ();
            List < AttendeeWrapper > attendeeInfoList = new List < AttendeeWrapper > ();
            String query = 'SELECT Id, Registered_Contact__r.FirstName, Registered_Contact__r.LastName, Registered_Contact__r.Phone, Registered_Contact__r.Email, Registered_Contact__r.BirthDate,Is_volunteer__c, Status__c, Neighbour_Event__r.CheckIn_Date_Time__c, Neighbour_Event__r.End_Date_Time__c FROM Event_Registration__c ';
            query += 'WHERE Neighbour_Event__c = ' + '\'' + eventId + '\'';
            // if (status != null) {
            //     query += 'AND Status__c = ' + '\'' + status + '\'';
            // }
            // if (searchKey != null && searchKey.length() > 0) {
            //     searchKey = '\'%' + String.escapeSingleQuotes(searchKey.trim()) + '%\'';
            //     query += ' AND (Registered_Contact__r.FirstName LIKE '+searchKey+' OR Registered_Contact__r.LastName LIKE '+searchKey+' OR'; 
            //     query += ' Registered_Contact__r.Email LIKE ' + searchKey + ')';
            //     query += ' WITH SECURITY_ENFORCED ';
            // }
            for (Event_Registration__c eventReg: Database.query(query)) {
                AttendeeWrapper attendeeInfo = new AttendeeWrapper();
                ContactWrapper contactInfo = new ContactWrapper();
                attendeeInfo.eventRegistrationId = eventReg.Id;
                attendeeInfo.eventRegistrationStatus = eventReg.Status__c;
                attendeeInfo.isVolunteer = eventReg.Is_volunteer__c;
                
                contactInfo.firstName = eventReg.Registered_Contact__r.FirstName;
                contactInfo.lastName = eventReg.Registered_Contact__r.LastName;
                contactInfo.phoneNo = eventReg.Registered_Contact__r.Phone;
                contactInfo.email = eventReg.Registered_Contact__r.Email;
                contactInfo.dob = eventReg.Registered_Contact__r.BirthDate;
                attendeeInfo.contactInformation = contactInfo;
                if (eventReg.Status__c != 'Attended' && eventReg.Neighbour_Event__r.CheckIn_Date_Time__c <= System.now() 
                && eventReg.Neighbour_Event__r.End_Date_Time__c > System.now()) {
                    contactInfo.showCheckin = false;
                } else {
                    contactInfo.showCheckin = true;
                }
                attendeeInfoList.add(attendeeInfo);
            }
            return attendeeInfoList;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

     /*
        * Method Name: sendEmailMethod
        * Purpose : send Custom Email to participants of an event .
        * Paramter : String jsonData, String eventId, list<Object> fileList, Boolean filePresent
        * Return : ResponseWrapper
        */  
    @AuraEnabled
    public static ResponseWrapper sendEmailMethod(String jsonData, String eventId, list<Object> fileList, Boolean filePresent) {
        NeighbourEventWrapper neWrapper = (NeighbourEventWrapper) JSON.deserialize(jsonData, NeighbourEventWrapper.class);
      
        List<String> lstVersionsToInsert = new List<String>();
         List<Messaging.EmailFileAttachment> efaList = new  List<Messaging.EmailFileAttachment> ();
       
        for (Object file : fileList) {
            FileInfo fileData = (FileInfo)JSON.deserialize(JSON.serialize(file), FileInfo.class);
            String base64File = EncodingUtil.urlDecode(fileData.VersionData, 'UTF-8');
            
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            efa.setFileName(fileData.Title);
            efa.setBody(EncodingUtil.base64Decode(base64File));
            efaList.add(efa);
        }
        String errorString = '';

        try {
            List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
          
            ContentVersion contentVersionRec = new ContentVersion();
            List<String> contactEmailsToNotify = new List<String> ();
            for(Event_Registration__c er :[SELECT Registered_Contact__r.Email,Is_volunteer__c FROM Event_Registration__c WHERE   Neighbour_Event__c =:eventId ]){
                if(neWrapper.volunteer && er.Is_volunteer__c){
                    contactEmailsToNotify.add(er.Registered_Contact__r.Email);
                }
                if(neWrapper.participants &&  er.Is_volunteer__c == false){
                    contactEmailsToNotify.add(er.Registered_Contact__r.Email);
                }
            }
            if(contactEmailsToNotify.size() < 1){
                return new ResponseWrapper(false, 'NO_EMAIL');
            }
            for (String contactEmail : contactEmailsToNotify) {
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setToAddresses(new List<String>{contactEmail});
                mail.setSubject(neWrapper.emailSubject );
                mail.setHtmlBody(neWrapper.emailBody);
                mail.setWhatId(eventId);
                mail.setSaveAsActivity(false);
                //  /**attaches file to email */
               
                // List<Messaging.EmailFileAttachment> attachments =efaList;
                mail.setFileAttachments(efaList);

                mailList.add(mail);
            }
            if (mailList.size() > 0) {
                List<Messaging.SendEmailResult> emailResult = Messaging.sendEmail(mailList);
                for (Messaging.SendEmailResult result : emailResult) {
                    if (!result.isSuccess()) {
                        String errorMessage = result.getErrors()[0].getMessage();
                        errorString += errorMessage;
                    }
                }
                if (errorString != '') {
                    return new ResponseWrapper(false, 'FAILURE_MESSAGE' + ' - ' + errorString);
                }
            }
            return new ResponseWrapper(true, 'SUCCESS_MESSAGE');
        } catch (Exception e) {
            return new ResponseWrapper(false, e.getMessage());
        }
        
    }
    public class ResponseWrapper {
        @AuraEnabled public boolean success;
        @AuraEnabled public String message;
        public ResponseWrapper(Boolean success, String message) {
            this.success = success;
            this.message = message;
        }
    }
    public class FileInfo {
        public String Title;
        public String VersionData;
    }
}  